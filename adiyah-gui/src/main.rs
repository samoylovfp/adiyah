use macroquad::prelude::*;
use shared::set_up_sound;

fn window_conf() -> Conf {
    Conf {
        window_title: "egui with macroquad".to_owned(),
        high_dpi: true,
        ..Default::default()
    }
}

#[macroquad::main(window_conf)]
async fn main() -> anyhow::Result<()> {
    let (_listener, spectro_consumer) = set_up_sound(1 << 11);

    set_default_camera();
    let render_target = render_target(1920, 1080);
    let shade = Texture2D::from_rgba8(1, 1, &[0, 0, 0, 10]);

    loop {
        let mut bucket = spectro_consumer.recv()?;
        blend_to_texture(render_target, &mut bucket, shade);
        draw_to_screen(render_target);
        next_frame().await;
    }
}

fn draw_to_screen(render_target: RenderTarget) {
    set_default_camera();
    draw_texture_ex(
        render_target.texture,
        0.0,
        0.0,
        WHITE,
        DrawTextureParams {
            dest_size: Some(Vec2::new(screen_width(), screen_height())),
            ..Default::default()
        },
    )
}

fn blend_to_texture(render_target: RenderTarget, bucket: &mut Vec<f32>, shade: Texture2D) {
    // This makes spectro go from 0 to 11 khz;
    let _ = bucket.split_off(bucket.len() / 4);

    let dx = 1.0 / bucket.len() as f32;

    set_camera(&Camera2D {
        rotation: 0.0,
        zoom: Vec2::new(2.0, 2.0),
        target: Vec2::new(0.0, 0.0),
        offset: Vec2::new(-1.0, -1.0),
        render_target: Some(render_target),
    });
    draw_texture_ex(
        shade,
        0.0,
        0.0,
        WHITE,
        DrawTextureParams {
            dest_size: Some(Vec2::new(100.0, 100.0)),
            ..Default::default()
        },
    );

    for (i, points) in bucket.windows(2).enumerate() {
        let volume = (points[1].max(points[0]) * 30.0).min(1.0);
        let color = Color::new(volume, 0.0, 1.0 - volume, 1.0);
        draw_line(
            i as f32 * dx,
            1.0 - points[0] * 10.0,
            (i + 1) as f32 * dx,
            1.0 - points[1] * 10.0,
            dx,
            color,
        );
    }
}
