use shared::set_up_sound;
use terminal_ui::Tui;

mod terminal_ui;

fn main() -> anyhow::Result<()> {
    let window_size = 1 << 11;
    let (_listener, spectro_consumer) = set_up_sound(window_size);
    let terminal = Tui::new();
    let _ignore_error = terminal.main_loop(spectro_consumer);
    Ok(())
}
