use std::{
    io::Stdout,
    sync::mpsc::{channel, Receiver, TryRecvError},
};

use shared::{analysis::Spectro, types::ProcessingStopped};
use termion::{
    event::Key,
    input::TermRead,
    raw::{IntoRawMode, RawTerminal},
};
use tui::{
    backend::{Backend, TermionBackend},
    layout::{Constraint, Direction, Layout},
    style::{Color, Style},
    symbols,
    widgets::{Axis, Block, Borders, Chart, Dataset},
    Terminal,
};

pub struct Tui {
    terminal: Terminal<TermionBackend<RawTerminal<Stdout>>>,
}

impl Tui {
    pub fn new() -> Self {
        let stdout = std::io::stdout().into_raw_mode().unwrap();
        let backend = TermionBackend::new(stdout);
        let terminal = Terminal::new(backend).unwrap();
        Self { terminal }
    }

    pub fn main_loop(
        mut self,
        spectro_consumer: Receiver<Spectro>,
    ) -> Result<(), ProcessingStopped> {
        self.terminal.clear().unwrap();
        let (key_in, key_out) = channel();
        std::thread::Builder::new()
            .name("keys_reader".to_string())
            .spawn(move || {
                let stdin = std::io::stdin();
                for k in stdin.keys() {
                    if key_in.send(k.unwrap()).is_err() {
                        return;
                    }
                }
            })
            .unwrap();

        loop {
            let mut bucket = spectro_consumer.recv()?;
            // This makes spectro go from 0 to 11 khz;
            let _ = bucket.split_off(bucket.len() / 4);
            draw(&mut self.terminal, &bucket);
            loop {
                match key_out.try_recv() {
                    Err(TryRecvError::Disconnected) | Ok(Key::Ctrl('c')) | Ok(Key::Char('q')) => {
                        return Ok(())
                    }
                    Err(TryRecvError::Empty) => break,
                    _ => {}
                }
            }
        }
    }
}

fn draw<B: Backend>(terminal: &mut Terminal<B>, data: &[f32]) {
    let max_value = 0.1;

    terminal
        .draw(|f| {
            let marked_data = data
                .iter()
                .enumerate()
                .map(|(i, v)| (i as f64, *v as f64))
                .collect::<Vec<(f64, f64)>>();

            let dataset = Dataset::default()
                .marker(symbols::Marker::Block)
                .style(Style::default().fg(Color::Cyan))
                .data(&marked_data);
            let chart = Chart::new(vec![dataset])
                .x_axis(Axis::default().bounds([0.0, data.len() as f64]))
                .y_axis(Axis::default().bounds([0.0, max_value]))
                .block(Block::default().borders(Borders::ALL));

            let chunks = Layout::default()
                .direction(Direction::Vertical)
                .margin(1)
                .constraints([Constraint::Ratio(1, 1)])
                .split(f.size());
            f.render_widget(chart, chunks[0]);
        })
        .unwrap();
}
