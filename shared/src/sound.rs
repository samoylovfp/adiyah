use std::sync::mpsc::Sender;

use cpal::{
    traits::{DeviceTrait, HostTrait, StreamTrait},
    SampleRate,
};

use cpal::Stream;

use crate::types::SoundBucket;

pub struct SoundListener {
    _stream: Stream,
}

impl SoundListener {
    pub fn new(sender: Sender<SoundBucket>) -> SoundListener {
        let host = cpal::default_host();
        let device = host
            .default_input_device()
            .expect("no output device available");

        let mut supported_configs_range = device
            .supported_output_configs()
            .expect("error while querying configs");

        let supported_config = supported_configs_range
            .next()
            .expect("no supported config?!")
            .with_sample_rate(SampleRate(44100))
            .config();

        let stream = device
            .build_input_stream(
                &supported_config,
                move |data: &[f32], _| {
                    let _ignore_error = sender.send(data.to_owned());
                },
                |e| eprintln!("Error when reading data: {:?}", e),
            )
            .unwrap();

        stream.play().unwrap();

        SoundListener { _stream: stream }
    }
}
