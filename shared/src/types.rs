use std::sync::mpsc::{RecvError, SendError};

pub type SoundBucket = Vec<f32>;
#[derive(Debug)]
pub struct ProcessingStopped;

impl<T> From<SendError<T>> for ProcessingStopped {
    fn from(_: SendError<T>) -> Self {
        ProcessingStopped
    }
}

impl From<RecvError> for ProcessingStopped {
    fn from(_: RecvError) -> Self {
        ProcessingStopped
    }
}
