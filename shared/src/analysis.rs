use std::{
    sync::mpsc::{Receiver, Sender},
    thread::{self},
};

use rustfft::{num_complex::Complex32, FftPlanner};

use crate::types::{ProcessingStopped, SoundBucket};

pub struct Gaborier {}

pub type Spectro = Vec<f32>;

impl Gaborier {
    pub fn spawn(
        sound_consumer: Receiver<SoundBucket>,
        spectro_producer: Sender<Spectro>,
        window_size: usize,
    ) -> Self {
        let _handle = thread::Builder::new()
            .name("Gaborier".to_owned())
            .spawn(move || -> Result<(), ProcessingStopped> {
                let fft = FftPlanner::<f32>::new().plan_fft_forward(window_size);

                let mut buf: SoundBucket = SoundBucket::new();

                let gauss_mask: Vec<f32> = (0..window_size)
                    .map(|x| x as f32 / (window_size - 1) as f32) // 0.0..=1.0
                    .map(|x| x * 2.0 - 1.0) // -1.0..1.0
                    .map(|x| gauss(x, 0.4))
                    .collect();

                let overlap = 0.4;
                let discard_samples = ((1.0 - overlap) * window_size as f32) as usize;

                loop {
                    while buf.len() < window_size {
                        let bucket = sound_consumer.recv()?;
                        buf.extend(bucket);
                    }
                    let mut complex: Vec<Complex32> = buf
                        .iter()
                        .take(window_size)
                        .zip(&gauss_mask)
                        .map(|(value, gauss)| Complex32 {
                            re: value * gauss,
                            im: 0.0,
                        })
                        .collect();

                    fft.process(&mut complex);

                    let spectro = complex
                        .iter()
                        .map(|c| c.norm_sqr().sqrt() / window_size as f32)
                        // Spectrogram goes 0 hz -> 44.1 khz -> 0 hz.
                        // Second half is almost an identical mirror of first one
                        // So we are not interested in it and remove it
                        .take(window_size / 2)
                        .collect();
                    spectro_producer.send(spectro)?;
                    buf = buf.split_off(discard_samples);
                }
            })
            .unwrap();

        Gaborier {}
    }
}

// width 0.4 looks about right
// http://www.fooplot.com/#W3sidHlwZSI6MCwiZXEiOiIyXigtKCgteC8wLjQpXjIpKSIsImNvbG9yIjoiIzAwMDAwMCJ9LHsidHlwZSI6MTAwMH1d
fn gauss(x: f32, width: f32) -> f32 {
    let power = (-x / width).powf(2.0);
    2.0f32.powf(-power)
}
