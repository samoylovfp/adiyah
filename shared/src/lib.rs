use std::sync::mpsc::{channel, Receiver};

use analysis::{Gaborier, Spectro};
use sound::SoundListener;

pub mod analysis;
pub mod sound;
pub mod types;

pub fn set_up_sound(window_size: usize) -> (SoundListener, Receiver<Spectro>) {
    let (sound_producer, sound_consumer) = channel();
    let stream = SoundListener::new(sound_producer);

    // Here each bucket contains a complete spectrogram
    let (spectro_producer, spectro_consumer) = channel();

    let _analyzer = Gaborier::spawn(sound_consumer, spectro_producer, window_size);
    (stream, spectro_consumer)
}
